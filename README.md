#TestWatchApplication#

### Description ###

This application tests the [silverbolt application](https://bitbucket.org/silverboltai/test-engineer-code-task/src/master/). he runs it in two processes,  
one watching a given folder to file changes, the other creates files in the folder.  

The application checks during running of cpu and memory usage.  
It also checks output results and accuracy of operations, and print a report to the console.

* To run the application:  
First download the [Test Engineer application](https://bitbucket.org/silverboltai/test-engineer-code-task/src/master/) and follow the instructions.  
Then:

 1. Clone this repository

 2. Open console in the cloned folder

 3. Run "npm install"

 4. Run "node ." to see the application CLI options

* to run a test the application need three arguments:  

 1. path of the silverbolt application  

 2. path of a specipic folder  

 3. amount of filse to generate  