"use strict";

const { spawn, fork } = require('child_process');
const analytics = require('./analytics');
const Commander = require('commander');
const fs = require('fs');
const pidusage = require('pidusage');

const args = Commander
  .usage('[option]')
  .description(`This application tests the Silverbolt appliction.
  The application runs a process that will be observed in changes that occur in a given folder.
  At the same time the application runs a process that will add files to the folder.
  The application analyzes the output based on usage of CPU and memory, runtime and readiness.
  To use the application, enter a path to the folder to look at. And insert several files to add.
  Also enter the path to the Silverbolt application `)
  .option('-p, --program-path <path>', 'path to the folder which contains the Silverbolt application')
  .option('-w, --path-watch <path>', 'Path to a folder to watch and to generate files')
  .option('-g, --generate-data <int>', 'Number of test files to generate, in the path given', x => parseInt(x, 10))
  .parse(process.argv);

if (!(args.pathWatch && fs.existsSync(args.pathWatch) && args.generateData && args.programPath && fs.existsSync(args.programPath))) {
  Commander.help();
  process.exit(-1);
}

console.log(__dirname);
const exeWatchProcess = spawn('node', [args.programPath ,'-w' ,args.pathWatch]);
const exeFileMaker = fork(`${__dirname}/forkExe.js`);

exeFileMaker.send({ p: args.programPath, w: args.pathWatch, g: args.generateData });

let outputData = '';

exeWatchProcess.stdout.on('data', data => outputData += data);
exeWatchProcess.stderr.on('data', data => outputData += data);

setInterval(function() {pidusage(exeWatchProcess.pid, (arr, info) => {
    if(info.cpu != 0) {
      console.log(`Memory usage: ${info.memory}, CPU usage: ${info.cpu}`);
      //console.log(`memory usage: ${info.memory}`);
    }
})}, 300);


exeFileMaker.on('message', msg => {
  if(msg === 'kill'){

    //killimg the wach process
    exeWatchProcess.kill();

    //order the output
    var arrOutput = analytics.splitByLines(outputData);
    var report = new analytics.report(args.generateData, arrOutput);
    report.printDetails();
    process.exit(0);
  }
});