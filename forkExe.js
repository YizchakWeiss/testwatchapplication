"use strict";

const { exec } = require('child_process');

let amountToTimeout = 0;
process.on('message', msg => {
    let command = `node ${msg.p} -o ${msg.w} -g ${msg.g}`;
    amountToTimeout = parseInt(msg.g) * 1000;
    exec(command);
    setTimeout(() => process.send('kill'), amountToTimeout);
});