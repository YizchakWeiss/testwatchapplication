
/**
 *  Returns the time that passed from the beginning of the first file check to the last. 
 */
module.exports.fileCheckingTime = fileCheckingTime;
function fileCheckingTime(outputArr){
    var firstTime = Number.MAX_SAFE_INTEGER;
    var lastTime = 0;
    for(i = 1;i < outputArr.length; ++i){
        var t_time = parseInt(outputArr[i].split('::',1));
        if(t_time < firstTime) firstTime = t_time;
        if(t_time > lastTime) lastTime = t_time;
    }
    return lastTime - firstTime;
} 

/**
 * split the string by the char '\n'
 */
function splitByLines(str){
    let temparr = str.split("\n");
    temparr.splice(temparr.length-1);
    return temparr;
}

/**
 * 
 * @param {*} sumfiles 
 * @param {*} time 
 */
function averageTime(sumfiles, time){
    return time/sumfiles;
}


function medianTime(data){
    var time = new Array();
    for (let i = 1; i < data.length-1; i++){
        var firstArr = data[i].split("::");
        var secondArr = data[i+1].split("::");
        if(firstArr[2] === secondArr[2] && firstArr[1] === '!ADDED!'){
            if(secondArr[1] != '!DIRTY!'){
                time.push(parseInt(secondArr[0]) - parseInt(firstArr[0]));
            }
            else{
                for(let i = data.length-1;i>0;i--){
                    secondArr = data[i].split("::");
                    if(firstArr[2] === secondArr[2] && secondArr[1] === '!REMOVED!'){
                        time.push(parseInt(secondArr[0]) - parseInt(firstArr[0]));
                        break;
                    }
                }
            }
        }
    }
    time.sort((a,b)=> a - b);
    return time[time.length/2];
}

function falsePositive(outputArr){
    var sumRemoved = 0;
    var sumCleanRemoved = 0;
    for (let i = 1; i < outputArr.length; i++){
        var detailsArr = outputArr[i].split("::");
        if(detailsArr[1] === '!REMOVED!'){
            sumRemoved ++;
            if(detailsArr[2].split('.')[1] == 'clean'){
                sumCleanRemoved ++;
            }
        }
    }
    return {
        sumRemoved : sumRemoved,
        precentFalseRemoved : (sumCleanRemoved*100)/sumRemoved,
        dirtyRemoved : sumRemoved - sumCleanRemoved,
        cleanRemoved : sumCleanRemoved
    }
}


class report{
    constructor (sumFiles,outputArr){
        this.time = fileCheckingTime(outputArr);
        this.median = medianTime(outputArr);
        this.average = averageTime(sumFiles,this.time);
        this.DetailsDeletions = falsePositive(outputArr);
    }
    printDetails(){
        console.log();
        console.log(`Work time in milli-seconds:`);
        console.log(`  All files = ${this.time}ms, Average = ${this.average}ms, Median ${this.median}ms`);
        console.log();
        console.log('Activity analysis for this test case: ');
        console.log(`  Removed filse: All = ${this.DetailsDeletions.sumRemoved}, Clean = ${this.DetailsDeletions.cleanRemoved}, False positive = ${this.DetailsDeletions.precentFalseRemoved}%`);
        console.log();
    }


}

module.exports = {
    splitByLines : splitByLines,
    medianTime : medianTime,
    averageTime : averageTime,
    report : report
} 
